// In the client code, below everything else
Template.partialTask.helpers({
	isOwner: function () {
		return this.owner === Meteor.userId();
	}
});

// In the client code, below everything else
Template.partialTask.events({
	"click .task-complete": function () {
		// Set the checked property to the opposite of its current value
		Meteor.call("setChecked", this._id, !this.checked);
	},
	"click .task-delete": function () {
		Meteor.call("deleteTask", this._id);
	},
	"click .task-private": function () {
		Meteor.call("setPrivate", this._id, !this.private);
	}
});
