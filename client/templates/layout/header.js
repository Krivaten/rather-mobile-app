Template.layoutHeader.events({
	"click .hide-completed": function (event) {
		Session.set("hideCompleted", !Session.get("hideCompleted"));
	}
});

Template.layoutHeader.helpers({
	incompleteCount: function () {
		return Tasks.find({checked: {$ne: true}}).count();
	},
	hideCompleted: function () {
		return Session.get("hideCompleted");
	}
});
